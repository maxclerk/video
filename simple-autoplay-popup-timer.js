const style = document.createElement("style");
style.innerHTML = `
  /* Style the popup box */
  #popup-box {
    position: fixed;
    top: 85%;
    left: 90%;
    transform: translate(-50%, -50%);
    width: 148px;
    height: 84.75px;
    background-color: #fff;
    border: 1px solid #ccc;
    padding: 5px;
  }
  .iframe-container {
    position: relative;
    width: 100%;
    padding-bottom: 56.25%; /* 16:9 aspect ratio */
  }
  .iframe-container iframe {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }
`;
document.head.appendChild(style);

const popupBox = document.createElement("div");
popupBox.id = "popup-box";
popupBox.style.display = "none";
const iframeContainer = document.createElement("div");
iframeContainer.className = "iframe-container";
const iframe = document.createElement("iframe");
iframe.id = "my-iframe";
iframe.src = "https://maxclerk.gitlab.io/video/simple_ads.html";
iframe.style.border = "none";
iframe.scrolling = "no";
iframe.frameBorder = 0;
iframeContainer.appendChild(iframe);
popupBox.appendChild(iframeContainer);
document.body.appendChild(popupBox);

setInterval(function() {
  const iframe = document.getElementById("my-iframe");
  iframe.src = iframe.src;
}, 30000);

window.addEventListener("load", function() {
  const popupBox = document.getElementById("popup-box");
  popupBox.style.display = "block";
});
